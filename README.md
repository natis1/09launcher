[![pipeline status](https://gitlab.com/2009scape/09launcher/badges/master/pipeline.svg)](https://gitlab.com/2009scape/09launcher/-/commits/master) 

**RS09 Launcher**

This downloads client updates and displays the latest changelogs for the live game in the launcher.

This is intended only for players of 2009scape.

If you are running a fork of the codebase do not report your bugs to me.
